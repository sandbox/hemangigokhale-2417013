/**
 * @file
 * Javascript and jQuery functions which are useful for the unlimited_field_remover module.
 */

(function($) {
  Drupal.behaviors.unlimited_field_remover = {
    attach: function(context, settings) {
      $('document').ready(function() {
        $('fieldset#edit-field-unlimited-und.form-wrapper').delegate('.unlimited_field_remover_button', 'click', function() {
          $('#edit-field-unlimited-und-table tr').remove();
          var table_id = $(row).parent('tbody').parent('table').attr('id');
          $('#' + table_id + ' tr.draggable:visible').each(function(index, element) {
            $(element).removeClass('odd').removeClass('even');
            if ((index % 2) == 0) {
              $(element).addClass('odd');
            } else {
              $(element).addClass('even');
            }
          });
        });
      });

    }
  }
})(jQuery);
